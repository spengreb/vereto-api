# Vereto API
Rails 5 API for Vereto Projects

Vereto NG is the first to integrate.
http://gogs.veretium.com/PromoStarr/vereto-ng

# Stack
This project uses:
 - Rails 5.1 API 
 - MariaDB/MySQL Database
 - Ruby 2.3+ recommended 

# Deployment

Run 
``` cap production deploy``` to start deployment process

# TO DO
- Email verification on sign up
- Comments to articles

# vouching

Player submits their name and person they want to vouch

Voucher is checked to be already in the whitelist 
and 
if the vouchee is in the mojang database 
and 
not already in the whitelist 
and
not already vouched for
and
voucher has not already tried to vouch for this person
and
voucher is not vouching for himself


| id  | voucher  | vouchee   |  accepted  |   |
|---|---|---|---|---|
| 0  | promostarr  | promostarr   | 1  |   |
|  1 | promostarr   | goblin   |   |   |
|  2 | goblin   | chaosg0d   |   |   |