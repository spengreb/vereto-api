class AuthenticationController < ApplicationController
  skip_before_action :authorize_request, only: :authenticate

  def authenticate
    auth_token =
        AuthenticateUser.new(auth_params[:email], auth_params[:password]).call
    user = User.find_by(email: auth_params[:email])

    response = user.as_json
    response[:auth_token] = auth_token
    json_response(response)
  end

  private

  def auth_params
    params.permit(:email, :password)
  end
end
