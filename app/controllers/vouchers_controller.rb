class VouchersController < ApplicationController
  before_action :set_voucher, only: [:show, :update, :destroy]
  skip_before_action :authorize_request, only: [:index, :show, :create]

  # GET /vouchers
  def index
    @vouchers = Voucher.all
    whitelist = `mc-whitelist-users`.gsub("\n", "")

    render json: { :vouchers => @vouchers, :whitelist => whitelist }.to_json
  end

  # GET /vouchers/1
  def show
    render json: @voucher
  end

  # POST /vouchers
  def create
    # Logic for creating a voucher
    output = ""
    whitelist = `mc-whitelist-users`.gsub("\n", "").split(", ")

    if Voucher.exists?(vouchee: params[:voucher]) && params[:voucher].in?(whitelist)
      @voucher = Voucher.new(voucher_params)
      
      if @voucher.save
        output = `mc-whitelist-users #{params[:vouchee]}`
        render json: { :message => output }.to_json, status: :created, location: @voucher
      else
       render json: @voucher.errors, status: :unprocessable_entity
      end
    else

      render json: { :message => "Voucher is not valid.", :whitelisted => params[:voucher].in?(whitelist), :exists => Voucher.exists?(vouchee: params[:voucher]), :list => whitelist }.to_json, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /vouchers/1
  def update
    if @voucher.update(voucher_params)
      render json: @voucher
    else
      render json: @voucher.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vouchers/1
  def destroy
    @voucher.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_voucher
      @voucher = Voucher.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def voucher_params
      params.permit(:voucher, :vouchee, :accepted)
    end
end
