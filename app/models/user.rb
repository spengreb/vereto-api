class User < ApplicationRecord
  has_secure_password

  has_many :todos, foreign_key: :created_by
  has_many :articles
  has_many :comments

  has_one  :user_profile
  
  validates_presence_of :name, :email, :password_digest
  validates_uniqueness_of :email
end
