class Voucher < ApplicationRecord

    validates_presence_of :voucher, :vouchee
    validates_uniqueness_of :vouchee
end
