class CreateUserProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_profiles do |t|
      t.string :avatar
      t.string :tagline
      t.date :dob
      t.string :role
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
