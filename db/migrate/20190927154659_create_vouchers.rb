class CreateVouchers < ActiveRecord::Migration[6.0]
  def change
    create_table :vouchers do |t|
      t.string :voucher
      t.string :vouchee
      t.boolean :accepted

      t.timestamps
    end
  end
end
