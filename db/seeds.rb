# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
30.times do |n|
  name = Faker::Name.name
  email = Faker::Internet.email
  password = "Muffins24"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password)
end

30.times do |n|
  title = Faker::Lovecraft.tome
  content = Faker::Lovecraft.paragraphs(5)
  Article.create!(title: title,
               post: content.to_s,
               user_id: 1)

end