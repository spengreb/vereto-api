Rails.application.routes.draw do
  resources :vouchers

  resources :comments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  scope module: :v2, constraints: ApiVersion.new('v2') do
    resources :articles
  end

  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :articles do
      resources :comments
    end
  end

  resources :users
  
  resources :user_profiles
  post 'login', to: 'authentication#authenticate'
  post 'register', to: 'users#create'

end
